<p align="center">
  <img src="pictures/OpenNept4une.png" width="350" alt="OpenNept4une Logo">
  <h1 align="center">OpenNept4une</h1>
  <h1 align="center">De-Elegoo-izing the Neptune 4 Series 3D Printers</h1>
</p>

---

<div align="center">

| 📥 Download | ⚙️ Installation | 💬 Discord Server |
|---------------------------------------------|---------------------------------------------|--------------------------------------------|
| [Releases](https://github.com/OpenNeptune3D/OpenNept4une/releases/) | [Wiki](https://github.com/OpenNeptune3D/OpenNept4une/wiki) | [Discord](https://discord.com/invite/X6kwchT6WM) |

</div>


---

<div align="center">
  
### Credits: (Community Members)  
Phillip Thelen, barrenechea, SQUIRRELYMOOSE, DanDonut & Jaerax

</div>

---
### Image Features
- 支持Neptune 4全系列型号 
- 自定义Armbian镜像 (24.2.0 Bookworm with Linux kernel 6.1.67) ([Credit](https://github.com/halfmanbear/Armbian-ZNP-K1-build))
- 附带Klipper v0.12 + MCU轻松烧录
- 可更新的服务、软件包和固件 (从此以后更新无需Elegoo固件)
- 触摸屏支持【测试版】 (响应更快)
- 移除所有Elegoo服务 (再无Z轴问题)
- 提供Orca Slicer配置
- 已配置自适应网床
- 便利的WiFi配置
- 精简而高度定制化的 printer.cfg
- 已预置LED控制、网床调平&PID校准宏
- 配置的螺钉和调整(?Screws Tilt Adjust Configured?) 
- 已配置Z轴扭曲补偿
- 已配置固件回滚
- Fluidd中可进行Linux上位机CPU 及 控制板MCU 温度监测
- 进一步优化的主板散热控制
- Crowsnest V4.X (Main) w/ ustreamer (网络摄像头服务端)
  
---

<div style="text-align: center;">
  <img src="pictures/main-page.png" width="450" alt="OpenNept4une Main-Page" style="margin-right: 10px;">
  <img src="pictures/advanced-page.png" width="450" alt="OpenNept4une Advanced-Page">
</div>

---

### Included Projects: 
  - [Armbian](https://github.com/armbian/build)
  - [(Fork) Armbian-ZNP-K1-build base image](https://github.com/OpenNeptune3D/Armbian-ZNP-K1-build)
  - [display_connector](https://github.com/OpenNeptune3D/display_connector)
  - [KAMP (Klipper-Adaptive-Meshing-Purging)](https://github.com/kyleisah/Klipper-Adaptive-Meshing-Purging)
  - [kiauh (Klipper Installation And Update Helper)](https://github.com/dw-0/kiauh)
  - [Klipper](https://github.com/Klipper3d/klipper)
  - [moonraker](https://github.com/Arksine/moonraker)
  - [fluidd:](https://github.com/fluidd-core/fluidd)
  - [mainsail](https://github.com/mainsail-crew/mainsail)
  - [crowsnest](https://github.com/mainsail-crew/crowsnest)
  - [mobileraker](https://github.com/Clon1998/mobileraker)


